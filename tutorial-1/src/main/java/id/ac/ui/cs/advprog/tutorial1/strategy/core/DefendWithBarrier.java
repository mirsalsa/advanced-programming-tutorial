package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "*wear barier*";
    }

    @Override
    public String getType() {
        return "Defense with barrier";
    }
}
