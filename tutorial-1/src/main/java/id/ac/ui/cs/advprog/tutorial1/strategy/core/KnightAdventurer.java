package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer(){
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "Knight";
    }
}
