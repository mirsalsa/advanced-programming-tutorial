package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer(){
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "Agile";
    }
}
