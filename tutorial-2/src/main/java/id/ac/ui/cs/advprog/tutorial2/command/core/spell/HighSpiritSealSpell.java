package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritSealSpell extends HighSpiritSpell {
    // TODO: Complete Me (ok)
    public HighSpiritSealSpell(HighSpirit spirit){
        super(spirit);
    }

    @Override
    public void cast() {
        this.spirit.seal();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
