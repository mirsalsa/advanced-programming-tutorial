package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.LinkedList;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;
    // TODO: Complete Me (ok)
    public FamiliarSpell (Familiar familiar){
        this.familiar = familiar;
    }

    @Override
    public void undo() {
        if (familiar.getPrevState() == FamiliarState.ACTIVE) familiar.summon(); else familiar.seal();
        // TODO: Complete Me (ok)
    }
}
