package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {
    int tpower;
    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            tpower = rawAge*2000;
            return rawAge*2000;
        } else if (rawAge <50) {
            tpower = rawAge*2250;
            return rawAge*2250;
        } else {
            tpower = rawAge*5000;
            return rawAge*5000;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    @Override
    public String powerClassifier() {
        if (tpower>0 && tpower<20000) {
            return "C class";
        } else if (tpower>20000 && tpower<100000) {
            return "B class";
        } else if (tpower > 100000) {
            return "A class";
        } else {
            return "Out of Range";
        }
    }
}
